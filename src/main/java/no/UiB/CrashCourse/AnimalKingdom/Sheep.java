package no.UiB.CrashCourse.AnimalKingdom;

/**
 * This is a Sheep! Baaah!
 */
public class Sheep{

    private static final String breed = "Sheep";
    private String name = breed;

    public Sheep(String name) {
        this.name = name;
    }
}
